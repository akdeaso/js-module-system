import Grid from "./gridModule.js";

const gridData = {
  columns: ["Name", "Email", "Phone Number"],
  data: [
    ["John", "john@example.com", "(353) 01 222 3333"],
    ["Mark", "mark@gmail.com", "(01) 22 888 4444"],
  ],
};

const grid = new Grid(gridData);
grid.render("grid");
